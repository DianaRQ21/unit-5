Rails.application.routes.draw do
  devise_for :admins
  devise_for :users, controllers: { sessions: 'users/sessions' }
  resources :centros do
  resources :alumnos 
  end
   root to: 'centros#index'

    get'/alumnos', to: 'alumnos#index', as: 'alumnos'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
